/*
 * KDE_RARS.CPP - Main Window of RARS in KDE 3
 *
 * History
 *  ver. 0.76 Oct 00 - CCDOC
 *  ver. 0.90 Feb 03 - KDE3
 *
 * @author    Marc Gueury <mgueury@synet.be>
 * @see:      C++ Coding Standard and CCDOC in help.htm
 * @version   0.76
 */

//--------------------------------------------------------------------------
//                           I N C L U D E
//--------------------------------------------------------------------------

#include <qaction.h>
#include <qmainwindow.h>
#include <qmenubar.h>
#include <qpopupmenu.h>
#include <qstatusbar.h>
#include <qtoolbar.h>
#include <qaccel.h>
#include <qnamespace.h>
#include <qdialog.h>
#include <qimage.h>
#include <qlabel.h>
#include <qpainter.h>
#include <qwidget.h>
#include <qtimer.h>
#include <qlistbox.h>
#include "../graphics/g_global.h"
#include "../graphics/g_global.h"

#include "kde_rars.h"
#include "kde_initcar.h"

#define ICON_PATH_SMALL "/usr/share/icons/Tango/22x22/"
#define ICON_PATH_LARGE "/usr/share/icons/Human/32x32/"

//--------------------------------------------------------------------------
//                           E X T E R N S
//--------------------------------------------------------------------------

extern QImage g_aImage[];
extern TView3D * g_View3D;

extern void GL_Initialize( int x, int y );

//--------------------------------------------------------------------------
//                             T Y P E S
//--------------------------------------------------------------------------

////
//// Class MyOpenGL
////
// This class can cause a problem when compiling with QT without OpenGL
// In that case, comment the following define (QT_OPENGL)
// Or recompile yourself QT (www.trolltech.com)

#define QT_OPENGL
#ifdef QT_OPENGL
class MyOpenGL : public QGLWidget
{
public:
  MyOpenGL( QWidget *parent, const char *name ): QGLWidget(parent,name)
  {
  }

protected:
  void initializeGL()
  {
  }

  void resizeGL( int w, int h )
  {
    if( g_View3D )
    {
      g_View3D->Init( w, h, 0, 0, true );

    }
  }

  void paintGL()
  {
    if( g_View3D)
    {
      g_View3D->DrawAll();
    }
  }
};
#else // QT_OPENGL
class MyOpenGL : public QWidget
{
public:
  MyOpenGL( QWidget *parent, const char *name ): QWidget(parent,name)
  {
  }
};
#endif // QT_OPENGL

//--------------------------------------------------------------------------
//                            G L O B A L
//--------------------------------------------------------------------------

long g_iCptMain = 0;
KdeInitCar * g_dlgInitCar;

//--------------------------------------------------------------------------
//                            Class MyImage
//--------------------------------------------------------------------------

MyImage::MyImage( int image_id, QWidget * parent ):QWidget(parent)
{
  m_iImageId = image_id;
  setBackgroundMode( NoBackground );
}

void MyImage::paintEvent(QPaintEvent *)
{
  QPainter paint(this);
  paint.drawImage(0, 0, g_aImage[m_iImageId], 0, 0, g_aImage[m_iImageId].width(), g_aImage[m_iImageId].height());
}

//--------------------------------------------------------------------------
//                            Class KdeRars
//--------------------------------------------------------------------------

/**
   * Convenience function to translate obsolete constructor call
   **/
  QAction* KdeRars::createQAction(const QString& text, const QString& menuText,
                         QKeySequence accel, QObject * parent,  const char * slot, QObject* slotparent)
                         {
                             QAction* action = new QAction(menuText, accel, parent, text);
                             connect(action, SIGNAL(activated()), slotparent, slot);
                             return action;
                         }
/**
   * Convenience function to translate obsolete constructor call (alternative version)
   **/
    QAction* KdeRars::createQAction(const QString & text, const QIconSet & icon, const QString & menuText,
                             QKeySequence accel, QObject * parent, const char * slot, QObject* slotparent)
                             {
                                 QAction* action = new QAction(icon, menuText, accel, parent, text);
                                connect(action, SIGNAL(activated()), slotparent, slot);
                                return action;
                             }


/**
 * Constructor: construct the main window
 */
KdeRars::KdeRars( const char * name ): QMainWindow( 0, name )
{
  m_offx = 0;
  m_offy = 0;

  setCaption("Rars [ET]");

  QToolBar *tb = new QToolBar( this );

//QPixmap * test = new QPixmap("/usr/share/icons/Human/16x16/actions/application-exit.png");
  QAction * quit = createQAction("Quit", QIconSet(QPixmap(ICON_PATH_SMALL "actions/exit.png")) ,
                     "&Quit", Key_Q, this, SLOT ( closeAllWindows()), qApp);
// from http://doc.qt.digia.com/3.3/qapplication.html#closeAllWindows
// when the last window is closed, the application should quit
    connect( qApp, SIGNAL( lastWindowClosed() ), qApp, SLOT( quit() ) );

  QAction * changeView = createQAction("Change View", "&Change View",
                         Key_V, this, SLOT( slotChangeView() ), this );
  QAction * viewClassic = createQAction("Mode: Classic", "Mode: &Classic", 0, this, SLOT( slotViewClassic() ), this );
  QAction * viewFullScreen = createQAction("Mode: OpenGL Full screen", "Mode: OpenGL &Full screen", 0, this, SLOT( slotViewFullScreen() ), this );
  QAction * viewWindow = createQAction("Mode: OpenGL &Window", "Mode: OpenGL &Window", 0, this, SLOT( slotViewWindow() ), this );
  QAction * viewTelemetry = createQAction("Mode: &Telemetry", "Mode: &Telemetry", 0, this, SLOT( slotViewTelemetry() ), this );

  QAction * viewZoom = createQAction("modezoom", "Mode: &Zoom", 0, this, SLOT( slotViewZoom()), this);

  QAction * viewTrajectory = createQAction("View/Hide &Trajectory", "View/Hide &Trajectory",
                         Key_T, this, SLOT( slotViewTrajectory() ), this );
  QAction * viewDriverNames = createQAction("Show &Names", "Show &Names",
                         Key_N, this, SLOT( slotViewDriverNames() ), this );
  QAction * carNext = createQAction("Next Car", QIconSet(QPixmap(ICON_PATH_SMALL "actions/go-up.png")),
                         "&Next Car", Key_PageUp, this, SLOT( slotCarNext() ), this );
  QAction * carPrevious = createQAction("&Previous Car", QIconSet(QPixmap(ICON_PATH_SMALL "actions/go-down.png")),
                         "&Previous Car", Key_PageDown, this, SLOT( slotCarPrevious() ), this );
  QAction * speedRewind = createQAction("&Rewind", QIconSet( QPixmap(ICON_PATH_SMALL "actions/player_rew.png")),
                         "&Rewind", Key_R, this, SLOT( slotSpeedRewind() ), this );
  QAction * speedStop = createQAction("Stop", QIconSet( QPixmap(ICON_PATH_SMALL "actions/player_stop.png")),
                         "Stop", Key_E, this, SLOT( slotSpeedStop() ), this );
  QAction * speedSlow = createQAction("&Slow", QIconSet( QPixmap(ICON_PATH_SMALL "actions/player_end.png")),
                         "&Slow", Key_S, this, SLOT( slotSpeedSlow() ), this );
  QAction * speedMedium = createQAction("&Medium", QIconSet( QPixmap(ICON_PATH_SMALL "actions/player_play.png")),
                         "&Medium", Key_D, this, SLOT( slotSpeedMedium() ), this );
  QAction * speedFast = createQAction("&Fast", QIconSet( QPixmap(ICON_PATH_SMALL "actions/player_fwd.png")),
                         "&Fast", Key_F, this, SLOT( slotSpeedFast() ), this );
  QAction * followCarManual = createQAction("Manual &Update", QIconSet( QPixmap("misc")),
                         "Manual &Update", Key_U, this, SLOT( slotFollowCarManual() ), this );
  QAction * followCarNobody = createQAction("Nobody", QIconSet( QPixmap("misc")),
                         "Nobody", Key_I, this, SLOT( slotFollowCarNobody() ), this );
  QAction * followCarOvertaking = createQAction("Any &Overtaking", QIconSet( QPixmap("misc")),
                         "Any &Overtaking", Key_O, this, SLOT( slotFollowCarOvertaking() ), this );
  QAction * followCarPosition = createQAction("For &Position", QIconSet( QPixmap("misc")),
                         "For &Position", Key_P, this, SLOT( slotFollowCarPosition() ), this );
  // Shift because bug in KDE 2.2 ('+' only does give shift+'+' ??)
  QAction * zoomIn = createQAction("zoomin", QIconSet(QPixmap(ICON_PATH_SMALL "actions/list-add.png")), "Zoom &In",
                         SHIFT+Key_Plus, this, SLOT( slotZoomIn()), this);
  QAction * zoomOut = createQAction("Zoom &Out", QIconSet(QPixmap(ICON_PATH_SMALL "actions/list-remove.png")),
                         "Zoom &Out", SHIFT+Key_Minus, this, SLOT( slotZoomOut() ), this );
  QAction * settingsSave = createQAction("Save Settings", "Save Settings", 0, this, SLOT( slotSettingsSave() ), this );
  QAction * moveUp = createQAction("Up", QIconSet( QPixmap(ICON_PATH_SMALL "actions/up.png")),
                         "Up", Key_Up, this, SLOT( slotMoveUp() ), this );
  QAction * moveDown = createQAction("Down", QIconSet( QPixmap(ICON_PATH_SMALL "actions/down.png")),
                         "Down", Key_Down, this, SLOT( slotMoveDown() ), this );
  QAction * moveLeft = createQAction("Left", QIconSet( QPixmap(ICON_PATH_SMALL "actions/back.png")),
                         "Left", Key_Left, this, SLOT( slotMoveLeft() ), this );
  QAction * moveRight = createQAction("Right", QIconSet( QPixmap(ICON_PATH_SMALL "actions/forward.png")),
                         "Right", Key_Right, this, SLOT( slotMoveRight() ), this );

  QPopupMenu * menuFile = new QPopupMenu;

  quit->addTo(menuFile);
  menuBar()->insertItem("&File", menuFile );

  QPopupMenu * menuView = new QPopupMenu;
  changeView->addTo(menuView);
  menuView->insertSeparator();
  viewClassic->addTo(menuView);
  viewFullScreen->addTo(menuView);
  viewWindow->addTo(menuView);
  viewTelemetry->addTo(menuView);
  viewZoom->addTo(menuView);
  menuView->insertSeparator();
  zoomIn->addTo(menuView);
  zoomOut->addTo(menuView);
  menuView->insertSeparator();
  viewTrajectory->addTo(menuView);
  viewDriverNames->addTo(menuView);
  menuView->insertSeparator();
  QPopupMenu * menuMove = new QPopupMenu;
  moveUp->addTo(menuMove);
  moveDown->addTo(menuMove);
  moveLeft->addTo(menuMove);
  moveRight->addTo(menuMove);
  menuView->insertItem("Move", menuMove );

  menuBar()->insertItem("&View", menuView );

  QPopupMenu * menuCar = new QPopupMenu;
  carNext->addTo(menuCar);
  carPrevious->addTo(menuCar);
  menuBar()->insertItem("&Car", menuCar );

  // insert menuFollowCar as submenu of menuCar
  menuCar->insertSeparator();
  QPopupMenu * menuFollowCar = new QPopupMenu;
  followCarManual->addTo(menuFollowCar);
  followCarNobody->addTo(menuFollowCar);
  followCarPosition->addTo(menuFollowCar);
  followCarOvertaking->addTo(menuFollowCar);
  menuCar->insertItem("Follow &Car", menuFollowCar );

  QPopupMenu * menuSpeed = new QPopupMenu;
  speedRewind->addTo(menuSpeed);
  speedStop->addTo(menuSpeed);
  menuSpeed->insertSeparator();
  speedSlow->addTo(menuSpeed);
  speedMedium->addTo(menuSpeed);
  speedFast->addTo(menuSpeed);
  menuBar()->insertItem("&Speed", menuSpeed );

  QPopupMenu * menuSettings = new QPopupMenu;
  settingsSave->addTo(menuSettings);
  menuBar()->insertItem("Settings", menuSettings );

  speedRewind->addTo(tb);
  speedStop->addTo(tb);
  speedSlow->addTo(tb);
  speedMedium->addTo(tb);
  speedFast->addTo(tb);
  carNext->addTo(tb);
  carPrevious->addTo(tb);
  zoomIn->addTo(tb);
  zoomOut->addTo(tb);

  statusBar()->message( "Ready!");

  main_part1();
  main_part2();

  this->setGeometry(0, 0, 624,510);
  widget_view = new QWidget( this );
  setCentralWidget(widget_view);
  InitDialog();
  // this->showMaximized();

  // create and setup a timer
  timer = new QTimer( this, "Timer" );
  connect( timer, SIGNAL( timeout() ), this, SLOT( slotTimeOut() ) );
  timer->start( 5, false );
}

/**
 * Slot: Next Car
 */
void KdeRars::slotCarNext()
{
  statusBar()->message( "Car: Next", 1000);
  g_iLastKey = UP;
}

/**
 * Slot: Previous Car
 */
void KdeRars::slotCarPrevious()
{
  statusBar()->message( "Car: Previous", 1000);
  g_iLastKey = DOWN;
}

/**
 * Slot: Speed rewind
 */
void KdeRars::slotSpeedRewind()
{
  statusBar()->message( "Speed: rewind", 1000);
  g_iLastKey = 'R';
}

/**
 * Slot: Speed slow
 */
void KdeRars::slotSpeedStop()
{
  statusBar()->message( "Speed: stop", 1000);
  g_iLastKey = 'E';
}

/**
 * Slot: Speed slow
 */
void KdeRars::slotSpeedSlow()
{
  statusBar()->message( "Speed: slow", 1000);
  g_iLastKey = 'S';
}

/**
 * Slot: Speed medium
 */
void KdeRars::slotSpeedMedium()
{
  statusBar()->message( "Speed: medium", 1000);
  g_iLastKey = 'D';
}

/**
 * Slot: Speed fast
 */
void KdeRars::slotSpeedFast()
{
  statusBar()->message( "Speed: fast", 1000);
  g_iLastKey = 'F';
}

/**
 * Slot: Change view in OpenGL mode
 */
void KdeRars::slotChangeView()
{
  statusBar()->message( "View: Change View", 1000);
  g_iLastKey = 'V';
}

/**
 * Slot: view in Zoom mode
 */
void KdeRars::slotViewClassic()
{
  statusBar()->message( "View: Mode Classic", 1000);
  SetOpenGLMode( MODE_CLASSIC );
}

/**
 * Slot: view to OpenGL Fullscreen Mode
 */
void KdeRars::slotViewFullScreen()
{
  statusBar()->message( "View: Mode Full screen", 1000);
  SetOpenGLMode( MODE_OPENGL_FULLSCREEN );
}

/**
 * Slot: view in Telemetry mode
 */
void KdeRars::slotViewTelemetry()
{
  statusBar()->message( "View: Mode Telemetry", 1000);
  SetOpenGLMode( MODE_TELEMETRY );
}

/**
 * Slot: view to OpenGL Window Mode
 */
void KdeRars::slotViewWindow()
{
  statusBar()->message( "View: Mode Window", 1000);
  SetOpenGLMode( MODE_OPENGL_WINDOW );
}

/**
 * Slot: view in Zoom mode
 */
void KdeRars::slotViewZoom()
{
  statusBar()->message( "View: Mode Zoom", 1000);
  SetOpenGLMode( MODE_ZOOM );
}

/**
 * Slot: view Trajectory
 */
void KdeRars::slotViewTrajectory()
{
  statusBar()->message( "View/Hide Trajectory", 1000);
  g_iLastKey = 'T';
}

/**
 * Slot: view driver names
 */
void KdeRars::slotViewDriverNames()
{
  statusBar()->message( "Show names", 1000);
  g_iLastKey = 'N';
}

/**
 * Slot: Follow Car Manual
 */
void KdeRars::slotFollowCarManual()
{
  statusBar()->message( "Follow Car: Manual Update", 1000);
  g_iLastKey = 'U';
}

/**
 * Slot: Follow Car Nobody
 */
void KdeRars::slotFollowCarNobody()
{
  statusBar()->message( "Follow Car: Nobody", 1000);
  g_iLastKey = 'I';
}

/**
 * Slot: Follow Car Overtaking
 */
void KdeRars::slotFollowCarOvertaking()
{
  statusBar()->message( "Follow Car: Any Overtaking", 1000);
  g_iLastKey = 'O';
}

/**
 * Slot: Follow Car Position
 */
void KdeRars::slotFollowCarPosition()
{
  statusBar()->message( "Follow Car: For Position", 1000);
  g_iLastKey = 'P';
}

/**
 * Slot: Zoom In
 */
void KdeRars::slotZoomIn()
{
  statusBar()->message( "Zoom: Zoom In", 1000);
  g_iLastKey = '+';
}

/**
 * Slot: Zoom out
 */
void KdeRars::slotZoomOut()
{
  statusBar()->message( "Zoom: Zoom Out", 1000);
  g_iLastKey = '-';
}

/**
 * Slot: Save Settings
 */
void KdeRars::slotSettingsSave()
{
  g_oRarsIni.SaveSettings();
  // Maybe show what was saved
}

/**
 * Slot: Move up, down, left, right,
 */
void KdeRars::slotMoveUp()
{
  g_iLastKey = ARROW_UP;
}
void KdeRars::slotMoveDown()
{
  g_iLastKey = ARROW_DOWN;
}
void KdeRars::slotMoveLeft()
{
  g_iLastKey = ARROW_LEFT;
}
void KdeRars::slotMoveRight()
{
  g_iLastKey = ARROW_RIGHT;
}

/**
 * Slot: Called by the timer 20 times per second
 */
void KdeRars::slotTimeOut()
{
  // The bActive trick is to allow the use of app.processEvents
  // in ShowInitMessage
  static bool bActive = false;
  if( bActive ) return;
  bActive = true;

  if( g_iCptMain==0 )
  {
     g_dlgInitCar = new KdeInitCar();
     g_dlgInitCar->ListMessage->clear();
     g_dlgInitCar->show();
  }
  else if( g_iCptMain==3 )
  {
     g_dlgInitCar->hide();
     delete g_dlgInitCar;
  }
  else
  {
    g_bToRefresh = false;
    while( !g_bToRefresh)
    {
      if( !main_part2() )
      {
        timer->stop();
        main_part3();
        statusBar()->message( "Race Finished !", 30000);
        return;
      }
    }

    int iViewMode = g_ViewManager->GetViewMode();
    if( iViewMode==MODE_OPENGL_FULLSCREEN )
    {
      image_opengl->update();
    }
    else if( iViewMode==MODE_CLASSIC || iViewMode==MODE_TELEMETRY )
    {
      image[0]->update();
    }
    else
    {
      if( iViewMode==MODE_OPENGL_WINDOW )
      {
        image_opengl->update();
      }
      else
      {
        image[0]->update();
      }
      image[1]->update();
      image[2]->update();
    }
  }
  g_iCptMain++;

  bActive = false;
}

/**
 * Called when the windows resizes
 */
void KdeRars::resizeEvent ( QResizeEvent * )
{
  int w = widget_view->width();
  int h = widget_view->height();
  m_offx = max( 0, w-624 );
  m_offy = max( 0, h-432 );  // and not 510 ??
  m_offx = m_offx-(m_offx%8);

  // <to_remove>
  g_ViewManager->DeleteAllViews();
  g_ViewManager->InitAllViews();
  // </to_remove>

  g_ViewManager->ResizeAllViews( m_offx, m_offy );
  SetImageGeometry();
}

void KdeRars::SetOpenGLMode( int mode )
{
//  timer->stop();
  g_ViewManager->DeleteAllViews();
  g_ViewManager->m_iViewMode = mode;
  g_ViewManager->InitAllViews();
  g_ViewManager->ResizeAllViews( m_offx, m_offy );
  SetImageGeometry();
//  timer->start( 50, false );
}

void KdeRars::InitDialog()
{
  image_opengl = new MyOpenGL(widget_view, "OpenGL");
  image[0] = new MyImage(0, widget_view);
  image[1] = new MyImage(1, widget_view);
  image[2] = new MyImage(2, widget_view);

  SetImageGeometry();
}

void KdeRars::SetImageGeometry()
{
  int iViewMode = g_ViewManager->GetViewMode();
  if( iViewMode==MODE_OPENGL_FULLSCREEN )
  {
    // wa problem resize
    image_opengl->hide();
    this->removeChild( image_opengl );
    delete image_opengl;
    image_opengl = new MyOpenGL(widget_view, "OpenGL");
    image_opengl->setGeometry( 0, 0, 624+m_offx, 432+m_offy );
    image_opengl->show();
    image[0]->hide();
    image[1]->hide();
    image[2]->hide();
  }
  else if( iViewMode==MODE_CLASSIC || iViewMode==MODE_TELEMETRY )
  {
    image[0]->setGeometry( 0, 0, 624+m_offx, 432+m_offy );
    image[0]->show();
    image_opengl->hide();
    image[1]->hide();
    image[2]->hide();
  }
  else
  {
    if( iViewMode==MODE_OPENGL_WINDOW )
    {
      // wa problem resize
      image_opengl->hide();
      this->removeChild( image_opengl );
      delete image_opengl;
      image_opengl = new MyOpenGL(widget_view, "OpenGL");
      image_opengl->setGeometry( 8, 8, 372+m_offx, 250+m_offy );
      image_opengl->show();
      image[0]->hide();
    }
    else
    {
      image[0]->setGeometry( 8, 8, 372+m_offx, 250+m_offy );
      image[0]->show();
      image_opengl->hide();
    }
    image[1]->setGeometry( 388+m_offx, 8, 220, 250+m_offy );
    image[2]->setGeometry( 8, 266+m_offy, 600+m_offx, 160 );
    image[1]->show();
    image[2]->show();
  }
}
