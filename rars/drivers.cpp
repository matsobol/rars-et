/**
 * DRIVERS.CPP - list of the drivers and their characteristics
 *
 * If you are changing number of drivers here, change also MAXCARS in car.h !!!
 * These are the control or "driver" programs which compete in the race:
 * See drivers[] below, and CAR.H
 *
 * @author    Mitchell E. Timin, State College, PA
 * @see:      C++ Coding Standard and CCDOC in help.htm
 * @version   0.76
 *
 * Geändert von Ingo Haschler für SE-Projekt ET09
 * ... und nochmals für ET10 (Nummerierung angepaßt)
 * ... und dann nochmals, um zwei Kurse zu unterstützen
 */

//--------------------------------------------------------------------------
//                           I N C L U D E
//--------------------------------------------------------------------------

#include "car.h"

//--------------------------------------------------------------------------
//                           G L O B A L S
//--------------------------------------------------------------------------
robot2 getGruppeA01Instance;
robot2 getGruppeA02Instance;
robot2 getGruppeA03Instance;
robot2 getGruppeA04Instance;
robot2 getGruppeA05Instance;
robot2 getGruppeA06Instance;
robot2 getGruppeA07Instance;
robot2 getGruppeA08Instance;
robot2 getGruppeA09Instance;
robot2 getGruppeA10Instance;
robot2 getGruppeA11Instance;
robot2 getGruppeA12Instance;
robot2 getGruppeA13Instance;
robot2 getGruppeA14Instance;
robot2 getGruppeA15Instance;
robot2 getGruppeA16Instance;
robot2 getGruppeA17Instance;
robot2 getGruppeA18Instance;
robot2 getGruppeA19Instance;

robot2 getGruppeB01Instance;
robot2 getGruppeB02Instance;
robot2 getGruppeB03Instance;
robot2 getGruppeB04Instance;
robot2 getGruppeB05Instance;
robot2 getGruppeB06Instance;
robot2 getGruppeB07Instance;
robot2 getGruppeB08Instance;
robot2 getGruppeB09Instance;
robot2 getGruppeB10Instance;
robot2 getGruppeB11Instance;
robot2 getGruppeB12Instance;
robot2 getGruppeB13Instance;
robot2 getGruppeB14Instance;
robot2 getGruppeB15Instance;
robot2 getGruppeB16Instance;
robot2 getGruppeB17Instance;
robot2 getGruppeB18Instance;
robot2 getGruppeB19Instance;

// Dozenten
robot2 getDoz1Instance;
robot2 getDoz2Instance;
robot2 getDoz3Instance;


/**
 * This is the permanent array of available drivers.
 *
 * Each bitmap name is associated with 2 colors.
 */
Driver * drivers[] =
{
    ////////////////////////////////////////////////////////////////////////////
    //  ET cars
    ////////////////////////////////////////////////////////////////////////////

    getGruppeA01Instance(),
    getGruppeA02Instance(),
    getGruppeA03Instance(),
    getGruppeA04Instance(),
    getGruppeA05Instance(),
    getGruppeA06Instance(),
    getGruppeA07Instance(),
    getGruppeA08Instance(),
    getGruppeA09Instance(),
    getGruppeA10Instance(),
    getGruppeA11Instance(),
    getGruppeA12Instance(),
    getGruppeA13Instance(),
    getGruppeA14Instance(),
    getGruppeA15Instance(),
    getGruppeA16Instance(),
    getGruppeA17Instance(),
    getGruppeA18Instance(),
    getGruppeA19Instance(),
    getGruppeB01Instance(),
    getGruppeB02Instance(),
    getGruppeB03Instance(),
    getGruppeB04Instance(),
    getGruppeB05Instance(),
    getGruppeB06Instance(),
    getGruppeB07Instance(),
    getGruppeB08Instance(),
    getGruppeB09Instance(),
    getGruppeB10Instance(),
    getGruppeB11Instance(),
    getGruppeB12Instance(),
    getGruppeB13Instance(),
    getGruppeB14Instance(),
    getGruppeB15Instance(),
    getGruppeB16Instance(),
    getGruppeB17Instance(),
    getGruppeB18Instance(),
    getGruppeB19Instance(),
	getDoz1Instance(),
	getDoz2Instance(),
	getDoz3Instance(),
};
